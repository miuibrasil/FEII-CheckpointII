
# FEII-Checkpoint-II

Olá! Este é o repositório do Checkpoint II da disciplina de Front-End II, do curso Certified Tech Developer @ DigitalHouse

# Equipe

- [Matheus Rayol](https://github.com/matheusrayol)
- [Henrique Santos](https://github.com/henriquec)
- [Fábio Neres](https://github.com/neresfabio)
- [Marcelo Magalhães](https://github.com/marcelomagal)
- [Pedro Marques](http://github.com/pedromarqs)

## Tema do Projeto

App To-Do: Aplicação Web para gerenciamento de tarefas

## Estrutura do Projeto

- index.html (Página de login)
- signup.html (Página de cadastro)
- tarefas.html (Página de tarefas na área logada)

## O que já foi feito

- Validações do formulário de login <br/>(*Mesa de trabalho, Matheus Ferreira, Fábio Neres, Henrique Santos*)
- Validações de execução da API de login<br/>(*Mesa de trabalho, Matheus Ferreira, Fábio Neres, Henrique Santos*)
- Validações do formulário de cadastro<br/>(*Pedro Marques*)
- Validações de execução da API de cadastro<br/>(*Mesa de Trabalho, Pedro Marques, Matheus Ferreira, Henrique Santos, Fábio Neres*)
- Inclusão de imagens personalizadas para etapas de login e cadastro<br/>(*Matheus Ferreira*)
- Estilização da página de login e da página de cadastro com Bootstrap 5<br/>(*Matheus Ferreira*)

## O que falta fazer

- Validações de execução da API na área logada (inclusão de tarefa, edição de tarefas, listagem de tarefas, logout, exibição de informações da conta, etc).
- Estilização da página de tarefas para os itens necessários.
- Outros requisitos ainda não apresentados.

## Opcional

- Inclusão de uma área extra em alguma página para a exibição do nome dos participantes do projeto.

# Direitos autorais

- <a  href="https://storyset.com/data">Data illustrations by Storyset</a>
- <a  href="https://storyset.com/user">User illustrations by Storyset</a>
- <a  href="https://storyset.com/online">Online illustrations by Storyset</a>
- <a  href='https://www.freepik.com/vectors/avatar-face'>Avatar face vector created by studiogstock - www.freepik.com</a>
